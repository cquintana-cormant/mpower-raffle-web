﻿using System.Data.Entity;
using TechIslandPFRaffle.Properties;

namespace TechIslandPFRaffle.Models
{
    public class FormModelContext : DbContext
    {
        public FormModelContext()
            : base("name=" + Settings.Default.DatabaseConnectionStringName)
        {
        }

        public virtual DbSet<FormModel> FormModels { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            var entity = modelBuilder.Entity<FormModel>();

            entity.ToTable("FormField");

            entity.HasKey(x => x.Id);

            entity.Property(c => c.Label);

            entity.Property(c => c.Value);

            entity.Property(c => c.TemplateFieldId);
        }  
    }
}

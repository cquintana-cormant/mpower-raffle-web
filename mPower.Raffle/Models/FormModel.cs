﻿namespace TechIslandPFRaffle.Models
{
    public class FormModel
    {
        public long Id { get; set; }

        public long FormId { get; set; }

        public long TemplateFieldId { get; set; }

        public string Label { get; set; }

        public string Value { get; set; }
    }
}

﻿module TechIslandPFRaffle.Controllers {

    export class HomeController {
        public winners: TechIslandPFRaffle.Models.Winner[] = [];

        public numberOfWinners: number = 0;

        public loading: boolean = false;

        public centerMessage: string;

        public showResults: boolean = false;

        constructor(private scope: ng.IScope, private restangular: restangular.IElement) {

            this.initialize();
        }


        public initialize() {
            this.numberOfWinners = 1;
            this.loading = false;
            this.showResults = false;
            this.winners = [];
        }

        public back() {
            this.initialize()
        }

        public getWinners() {
            this.winners = [];
            this.centerMessage = '';
            this.loading = true;
            this.showResults = true;
            this.restangular.one('api/form', this.numberOfWinners)
                .all('winners')
                .post(this.numberOfWinners)
                .then((data) => {
                this.centerMessage = this.numberOfWinners > 1 ? "AND THE WINNERS ARE: " : "AND THE WINNER IS: ";

                angular.forEach(data, formCollection => {

                    var winner = new TechIslandPFRaffle.Models.Winner();
                    winner.id = formCollection.FormId;
                    winner.name = _.pluck(_.where(formCollection.Forms, { 'Label': 'Full name' }), 'Value')[0];
                    winner.company = _.pluck(_.where(formCollection.Forms, { 'Label': 'Company Name' }), 'Value')[0];
                    winner.email = _.pluck(_.where(formCollection.Forms, { 'Label': 'Email Address' }), 'Value')[0];

                    this.winners.push(winner);
                });
                this.loading = false;

            },
                (ex) => {
                    this.loading = false;
                    console.log(ex);
                });
        }
    }

    angular.module('techIslandPFRaffle')
        .controller('HomeController', ['$scope', 'Restangular', HomeController]);
}   
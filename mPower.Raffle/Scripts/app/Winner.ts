﻿module TechIslandPFRaffle.Models {
    export class Winner {

        public id: number = 0;

        public name: string = '';

        public company: string = '';

        public email: string = '';

        constructor() { }
    }
} 
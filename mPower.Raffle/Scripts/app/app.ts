﻿module TechIslandPFRaffle {

    var app = angular.module('techIslandPFRaffle', [
        'ngResource',
        'ngRoute',
        'restangular'
    ]);

    // configure routing
    app.config([
        '$routeProvider', (routeProvider: ng.route.IRouteProvider) => {
            routeProvider
                .when('/home', {
                    templateUrl: '/PartialViews/Index.html',
                    controller: 'HomeController',
                    controllerAs: 'homeCtrl'
                })
                .otherwise({
                    redirectTo: '/home'
                });
        }
    ]);
} 
var TechIslandPFRaffle;
(function (TechIslandPFRaffle) {
    var app = angular.module('techIslandPFRaffle', [
        'ngResource',
        'ngRoute',
        'restangular'
    ]);
    // configure routing
    app.config([
        '$routeProvider',
        function (routeProvider) {
            routeProvider.when('/home', {
                templateUrl: '/PartialViews/Index.html',
                controller: 'HomeController',
                controllerAs: 'homeCtrl'
            }).otherwise({
                redirectTo: '/home'
            });
        }
    ]);
})(TechIslandPFRaffle || (TechIslandPFRaffle = {}));
//# sourceMappingURL=app.js.map
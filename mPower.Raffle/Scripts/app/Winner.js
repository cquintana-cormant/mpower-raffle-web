var TechIslandPFRaffle;
(function (TechIslandPFRaffle) {
    var Models;
    (function (Models) {
        var Winner = (function () {
            function Winner() {
                this.id = 0;
                this.name = '';
                this.company = '';
                this.email = '';
            }
            return Winner;
        })();
        Models.Winner = Winner;
    })(Models = TechIslandPFRaffle.Models || (TechIslandPFRaffle.Models = {}));
})(TechIslandPFRaffle || (TechIslandPFRaffle = {}));
//# sourceMappingURL=Winner.js.map
var TechIslandPFRaffle;
(function (TechIslandPFRaffle) {
    var Controllers;
    (function (Controllers) {
        var HomeController = (function () {
            function HomeController(scope, restangular) {
                this.scope = scope;
                this.restangular = restangular;
                this.winners = [];
                this.numberOfWinners = 0;
                this.loading = false;
                this.showResults = false;
                this.initialize();
            }
            HomeController.prototype.initialize = function () {
                this.numberOfWinners = 1;
                this.loading = false;
                this.showResults = false;
                this.winners = [];
            };
            HomeController.prototype.back = function () {
                this.initialize();
            };
            HomeController.prototype.getWinners = function () {
                var _this = this;
                this.winners = [];
                this.centerMessage = '';
                this.loading = true;
                this.showResults = true;
                this.restangular.one('api/form', this.numberOfWinners).all('winners').post(this.numberOfWinners).then(function (data) {
                    _this.centerMessage = _this.numberOfWinners > 1 ? "AND THE WINNERS ARE: " : "AND THE WINNER IS: ";
                    angular.forEach(data, function (formCollection) {
                        var winner = new TechIslandPFRaffle.Models.Winner();
                        winner.id = formCollection.FormId;
                        winner.name = _.pluck(_.where(formCollection.Forms, { 'Label': 'Full name' }), 'Value')[0];
                        winner.company = _.pluck(_.where(formCollection.Forms, { 'Label': 'Company Name' }), 'Value')[0];
                        winner.email = _.pluck(_.where(formCollection.Forms, { 'Label': 'Email Address' }), 'Value')[0];
                        _this.winners.push(winner);
                    });
                    _this.loading = false;
                }, function (ex) {
                    _this.loading = false;
                    console.log(ex);
                });
            };
            return HomeController;
        })();
        Controllers.HomeController = HomeController;
        angular.module('techIslandPFRaffle').controller('HomeController', ['$scope', 'Restangular', HomeController]);
    })(Controllers = TechIslandPFRaffle.Controllers || (TechIslandPFRaffle.Controllers = {}));
})(TechIslandPFRaffle || (TechIslandPFRaffle = {}));
//# sourceMappingURL=HomeController.js.map
﻿using System.Web.Mvc;
using TechIslandPFRaffle.Properties;

namespace TechIslandPFRaffle.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Title = Settings.Default.Title;
            return View();
        }

    }
}
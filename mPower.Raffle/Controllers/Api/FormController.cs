﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using TechIslandPFRaffle.Models;
using TechIslandPFRaffle.Properties;

namespace TechIslandPFRaffle.Controllers.Api
{
    [RoutePrefix("api/form")]
    public class FormController : ApiController
    {
        [Route("{numberOfWinners}/winners")]
        [HttpPost]
        public async Task<IHttpActionResult> GetWinners(int numberOfWinners)
        {
            List<FormCollection> result;
            try
            {
                var templateFieldIds = Settings.Default.TemplateFieldIds;

                if (string.IsNullOrWhiteSpace(templateFieldIds)) 
                    throw new Exception("Template field ids are required.");

                using (var db = new FormModelContext())
                {
                    var registrantCount = from form in db.FormModels
                        where templateFieldIds.Contains(form.TemplateFieldId.ToString())
                        select form.TemplateFieldId;

                    var totalCount = registrantCount.Count();

                    var query = from form in db.FormModels
                        where templateFieldIds.Contains(form.TemplateFieldId.ToString())
                        group form by form.FormId
                        into g
                        select new FormCollection
                        {
                            FormId = g.Key,
                            Forms = g.ToList()
                        };

                    result = await query.OrderBy(x => Guid.NewGuid())
                        .Take(numberOfWinners > totalCount ? totalCount : numberOfWinners)
                        .ToListAsync();
                }
            }
            catch (Exception ex)
            {
                return this.BadRequest(ex.Message);
            }

            return this.Ok(result);
        }
    }

    class FormCollection
    {
        public long FormId { get; set; }
        public ICollection<FormModel> Forms { get; set; } 
    }
}
